include $(MAK_DIR)/cc/msc.mak
include $(MAK_DIR)/cc/gcc.mak

USE_CC?=GCC
CC=$($(USE_CC)_CC)
CL=$($(USE_CC)_CL)
CC_c=$($(USE_CC)_c)
CC_D=$($(USE_CC)_D)
CC_CL_E=$($(USE_CC)_CL_E)
CC_I=$($(USE_CC)_I)
CC_L=$($(USE_CC)_L)
CC_l=$($(USE_CC)_l)
CC_o=$($(USE_CC)_o)
